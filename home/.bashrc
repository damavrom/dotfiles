# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Prompts
#PS1='[\u@\h \W]\$ '
export PS1='`prompt.sh` $ '
export PS2='...'

# Global Variables`
export EDITOR=/usr/bin/vim
export BROWSER=/usr/bin/qutebrowser
export PATH=${HOME}/.local/bin:${PATH}
export HISTSIZE=2000
export HISTIGNORE=' *'
#window title
#export PROMPT_COMMAND="printf \"\033]0;terminal\007\"; unset PROMPT_COMMAND"

# Miscellaneous Options
set -o vi

# Aliases
#alias ls='ls --color=auto'
