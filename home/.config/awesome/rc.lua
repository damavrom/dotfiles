-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Error handling -------------------------------------------------------------
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors 
    })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function(err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end

-- Variable definitions -------------------------------------------------------
-- Themes define colours, icons, font and wallpapers.
beautiful.init("~/.config/awesome/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "urxvt"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal.." -e "..editor

-- Default modkey.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    awful.layout.suit.corner.nw,
    awful.layout.suit.floating,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}

-- Menu -----------------------------------------------------------------------
--[[
myawesomemenu = {
   { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end},
   { "manual", terminal.." -e man awesome" },
   { "edit config", editor_cmd.." "..awesome.conffile},
   { "restart", awesome.restart},
   { "quit", function() awesome.quit() end},
}

mymainmenu = awful.menu({items = {
    {"awesome", myawesomemenu, beautiful.awesome_icon},
    { "open terminal", terminal}
}})

mylauncher = awful.widget.launcher({
    image = beautiful.awesome_icon,
    menu = mymainmenu
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
--]]

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- Wibar ----------------------------------------------------------------------
-- Create a textclock widget
mytextclock = wibox.widget.textclock("%A %-d/%-m - %H:%M", 10)

timer = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
    { 
        font = beautiful.font_miniscule,
        markup = "timer: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        "show_timer.sh",
        5,
        function(w, s)
            if #s > 0 then 
                timer.visible = true
                w:set_text(s)
            else
                timer.visible = false
                w:set_text("-")
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 0,
    },
}

podcasts = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
    { 
        font = beautiful.font_miniscule,
        markup = "pods: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        'bash -c "podcasts.py check 2> /dev/null | wc -l"',
        60,
        function(w, s)
            val = tonumber(s)
            w:set_text(val)
            if val > 0 then 
                podcasts.visible = true
            else
                podcasts.visible = false
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 0,
    },
}
outdated = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
    { 
        font = beautiful.font_miniscule,
        markup = "updates: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        "outdated.sh 500",
        60,
        function(w, s)
            if #s > 0 then 
                outdated.visible = true
                w:set_text(s)
            else
                outdated.visible = false
                w:set_text("-")
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 0,
    },
}
music = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
    { 
        font = beautiful.font_miniscule,
        markup = "playing: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        "music_info.sh",
        10,
        function(w, s)
            if #s > 0 then 
                music.visible = true
                w:set_text(s)
            else
                music.visible = false
                w:set_text("-")
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 0,
    },
}
devices = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    { 
        font = beautiful.font_miniscule,
        markup = "mnts: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        "devices.sh",
        10,
        function(w, s)
            if #s > 0 then 
                if #s > 10 then 
                    w:set_text(string.sub(s, 0, 7).."...")
                else
                    w:set_text(s)
                end
                devices.visible = true
            else
                devices.visible = false
                w:set_text("-")
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
}
top_cpu = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    { 
        font = beautiful.font_miniscule,
        markup = "top cpu: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        'bash -c "ps -eo comm,pcpu,times --sort=-pcpu,-time --no-headers | awk \'{if ($2 > 5) {if ($3 > 10) {print $1; exit;} else {next;}} else{exit;}}\'"',
        10,
        function(w, s)
            if #s > 0 then 
                if #s > 10 then 
                    w:set_text(string.sub(s, 0, 7).."...")
                else
                    w:set_text(s)
                end
                top_cpu.visible = true
            else
                top_cpu.visible = false
                w:set_text("-")
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
}
top_mem = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    { 
        font = beautiful.font_miniscule,
        markup = "top mem: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        'bash -c "ps -eo comm,pmem --sort=-rss,-time --no-headers | awk \'{if ($2 > 10) print $1; exit}\'"',
        10,
        function(w, s)
            if #s > 0 then 
                if #s > 10 then 
                    w:set_text(string.sub(s, 0, 7).."...")
                else
                    w:set_text(s)
                end
                top_mem.visible = true
            else
                top_mem.visible = false
                w:set_text("-")
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
}
sysload = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    { 
        font = beautiful.font_miniscule,
        markup = "sysload: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        "load_pretty.sh 0.5",
        10,
        function(w, s)
            if #s > 0 then 
                sysload.visible = true
                w:set_text(s)
            else
                sysload.visible = false
                w:set_text("-")
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
}
mem = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    { 
        font = beautiful.font_miniscule,
        markup = "ram: ",
        widget = wibox.widget.textbox
    },
    awful.widget.watch(
        "memory_pretty.sh 20",
        10,
        function(w, s)
            if #s > 0 then 
                mem.visible = true
                w:set_text(s)
            else
                mem.visible = false
                w:set_text("-")
            end
        end
    ),
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
}
traffic_down_text = wibox.widget {
    widget = wibox.widget.textbox,
    markup = "-",
}
traffic_down = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    { 
        font = beautiful.font_miniscule,
        markup = "dwnld spd: ",
        widget = wibox.widget.textbox
    },
    traffic_down_text,
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
    last_measurement = nil
}
awful.widget.watch("net_traffic.sh", 5, function(w, s)
    val = tonumber(s)
    diff = -1
    show = true
    if not val then 
        traffic_down.last_measurement = nil
    else
        if not traffic_down.last_measurement then
            traffic_down.last_measurement = val
        else
            diff = (val-traffic_down.last_measurement)/5
            traffic_down_text.markup = diff
            traffic_down.last_measurement = val
        end
    end
    if diff < 10*2^10 then
        traffic_down.visible = false
    elseif diff < 2^20 then
        traffic_down_text.markup = (math.floor(diff/(2^10))).."KiB/s"
        traffic_down.visible = true
    else
        traffic_down_text.markup = (math.floor(10*diff/(2^20))/10).."MiB/s"
        traffic_down.visible = true
    end
end)
traffic_up_text = wibox.widget {
    widget = wibox.widget.textbox,
    markup = "-",
}
traffic_up = wibox.widget {
    layout = wibox.layout.fixed.horizontal,
    visible = false,
    { 
        font = beautiful.font_miniscule,
        markup = "upld spd: ",
        widget = wibox.widget.textbox
    },
    traffic_up_text,
    wibox.widget {
        widget = wibox.widget.separator,
        color = beautiful.fg_dim,
        forced_width = 15,
        orientation = "vertical",
    },
    last_measurement = nil
}
awful.widget.watch("net_traffic.sh up", 5, function(w, s)
    val = tonumber(s)
    diff = -1
    show = true
    if not val then 
        traffic_up.last_measurement = nil
    else
        if not traffic_up.last_measurement then
            traffic_up.last_measurement = val
        else
            diff = (val-traffic_up.last_measurement)/5
            traffic_up_text.markup = diff
            traffic_up.last_measurement = val
        end
    end
    if diff < 10*2^10 then
        traffic_up.visible = false
    elseif diff < 2^20 then
        traffic_up_text.markup = (math.floor(diff/(2^10))).."KiB/s"
        traffic_up.visible = true
    else
        traffic_up_text.markup = (math.floor(10*diff/(2^20))/10).."MiB/s"
        traffic_up.visible = true
    end
end)

-- Create a wibox for each screen and add it

local function set_wallpaper(s)
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        gears.wallpaper.set(wallpaper)
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        -- gears.wallpaper.maximized(wallpaper, s, false)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    if screen.primary.index ~= s.index then
        awful.tag({"1", "2", "3", "4", "5", "6", "7", "8", "9"}, s, awful.layout.layouts[1])
    else
        awful.tag.add("1:http", {
            screen = s, layout = awful.layout.suit.fair,
            layout = awful.layout.suit.tile.right,
            gap = beautiful.useless_gap*4
        })
        awful.tag.add("2:mail", {
            screen = s, layout = awful.layout.suit.fair,
            gap = beautiful.useless_gap*4
        })
        awful.tag.add("3:misc", {
            screen = s, layout = awful.layout.suit.fair,
            gap = beautiful.useless_gap*4
        })
        awful.tag.add("4:misc", {screen = s, layout = awful.layout.suit.fair})
        awful.tag.add("5:video", {
            screen = s,
            layout = awful.layout.suit.tile.bottom,
            master_width_factor = 0.8,
        })
        awful.tag.add("6:term", {
            screen = s,
            layout = awful.layout.suit.tile.right,
            master_width_factor = 0.35,
            column_count = 2,
        })
        awful.tag.add("7:term", {
            screen = s,
            layout = awful.layout.suit.tile.right,
            master_width_factor = 0.35,
            column_count = 2
        })
        awful.tag.add("8:term", {screen = s, layout = awful.layout.suit.fair})
        awful.tag.add("9:term", {
            screen = s,
            layout = awful.layout.suit.tile.right,
            master_width_factor = 0.35,
            column_count = 1,
            gap = beautiful.useless_gap*4
        })
    end
    s.tags[1]:view_only()

    -- Create a promptbox for each screen
    s.prompt = awful.widget.prompt {
        prompt = '$ ',
        font = beautiful.font_prominent,
        done_callback = function() 
            s.popup.visible = false
        end,
        bg = beautiful.fg_dim,
        fg = beautiful.fg_focus,
        bg_cursor = beautiful.bg_alt,
        fg_cursor = beautiful.fg_alt,
    }
    s.popup = awful.popup {
        visible = false,
        screen = s,
        type = "normal",
        shape = rrect,
        ontop = true,
        bg = beautiful.fg_dim,
        widget = {
            widget = wibox.container.margin,
            margins = 10,
            s.prompt
        },
        placement = (awful.placement.center_horizontal),
        y = s.geometry.height*2//3
    }
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    --[[
    s.mylayoutbox:buttons(gears.table.join(
        awful.button({}, 1, function() awful.layout.inc(1) end),
        awful.button({}, 3, function() awful.layout.inc(-1) end),
        awful.button({}, 4, function() awful.layout.inc(1) end),
        awful.button({}, 5, function() awful.layout.inc(-1) end)
    ))
    --]]
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen = s,
        filter = awful.widget.taglist.filter.noempty,
        -- buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.activetask = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.focused
    }
    s.mintasks = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.minimizedcurrenttags,
        layout = {
            layout = wibox.layout.fixed.horizontal,
            spacing = 15,
            spacing_widget = wibox.widget {
                widget = wibox.widget.separator,
                color = beautiful.fg_dim,
                orientation = "vertical"
            },
        },
        widget_template = {
            id = "text_role",
            widget = wibox.widget.textbox,
        }
    }

    -- Create the wibox
    s.mywibox = awful.wibar({
        screen = s,
        position = "top",
        type = "desktop"
    })

    -- Add widgets to the wibox
    s.mywibox:setup {
        widget = wibox.container.margin,
        margins = 5,
        {
            layout = wibox.layout.align.horizontal,
            expand = "none",
            {
                layout = wibox.layout.fixed.horizontal,
                spacing = 15,
                spacing_widget = wibox.widget {
                    widget = wibox.widget.separator,
                    color = beautiful.fg_dim,
                    orientation = "vertical"
                },
                s.mylayoutbox,
                -- mylauncher,
                s.mytaglist,
                s.activetask,
                s.mintasks, 
            },
            {
                widget = wibox.container.margin,
                left = 40,
                right = 40,
                {
                    layout = wibox.layout.fixed.horizontal,
                    mytextclock,
                    music,
                    timer,
                    outdated,
                    podcasts
                },
            },
            {
                fg_color = beautiful.fg_focus,
                layout = wibox.layout.fixed.horizontal,
                traffic_down,
                traffic_up,
                devices,
                top_cpu,
                top_mem,
                sysload,
                mem,
                mykeyboardlayout,
                wibox.widget.systray(),
            },
        }
    }
    --[[
    s.barshadow = awful.wibox({
        screen = s,
        type = "desktop",
        bg = "linear:,0,0:0,10,:0,#333:0.1,#3338:1,#3330",
        height = 30, 
        position = awful.placement.next_to(s.mywibox),
    })
    s.barshadow:struts({top = 0, bottom = 0, left= 0, right = 0})
    --]]
    s.flower = awful.popup {
        screen = s,
        visible = true,
        type = "desktop",
        maximum_height = s.geometry.height//2,
        widget = wibox.widget {
            widget = wibox.widget.imagebox,
            image = beautiful.flower_image,
        },
        -- placement = awful.placement.centered,
        x = s.geometry.width*3//5,
        y = s.geometry.height//4
    }
end)


-- Mouse bindings -------------------------------------------------------------
--[[
root.buttons(gears.table.join(
    -- awful.button({}, 3, function() mymainmenu:toggle() end),
    awful.button({}, 4, awful.tag.viewnext),
    awful.button({}, 5, awful.tag.viewprev)
))
--]]

-- Key bindings ---------------------------------------------------------------
globalkeys = gears.table.join(
    awful.key(
        {modkey}, "s",
        hotkeys_popup.show_help,
        {description="show help", group="awesome"}
    ),
    awful.key(
        {modkey, "Control"}, "j",
        function() 
            local t = awful.screen.focused().selected_tag
            local tags = awful.screen.focused().tags
            n = #tags
            for i = 0, n-1 do
                local idx = (t.index+i)%n+1
                if #(tags[idx]:clients()) > 0 then
                    tags[idx]:view_only()
                    return
                end
            end
            awful.tag.viewnext()
        end,
        {description = "view previous", group = "tag"}
    ),
    awful.key(
        {modkey, "Control"}, "k",
        function() 
            local t = awful.screen.focused().selected_tag
            local tags = awful.screen.focused().tags
            n = #tags
            for i = 2, n+1 do
                local idx = (n+t.index-i)%n+1
                if #(tags[idx]:clients()) > 0 then
                    tags[idx]:view_only()
                    return
                end
            end
            awful.tag.viewnext()
        end,
        {description = "view next", group = "tag"}
    ),
    awful.key(
        {modkey}, "Escape",
        awful.tag.history.restore,
        {description = "go back", group = "tag"}
    ),
    awful.key(
        {modkey}, "j",
        function() awful.client.focus.byidx(1) end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key(
        {modkey}, "k",
        function() awful.client.focus.byidx(-1) end,
        {description = "focus previous by index", group = "client"}
    ),
    --[[
    awful.key(
        {modkey}, "w",
        function() mymainmenu:show() end,
        {description = "show main menu", group = "awesome"}
    ),
    --]]

    -- Layout manipulation
    awful.key(
        {modkey, "Shift"}, "j",
        function() awful.client.swap.byidx(1) end,
        {description = "swap with next client by index", group = "client"}
    ),
    awful.key(
        {modkey, "Shift"}, "k",
        function() awful.client.swap.byidx(-1) end,
        {description = "swap with previous client by index", group = "client"}
    ),
    awful.key(
        {modkey}, "u",
        awful.client.urgent.jumpto,
        {description = "jump to urgent client", group = "client"}
    ),
    awful.key(
        {modkey}, "Tab",
        function()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}
    ),

    -- Standard program
    awful.key(
        {modkey}, "Return",
        function() awful.spawn(terminal) end,
        {description = "open a terminal", group = "launcher"}
    ),
    awful.key(
        {modkey, "Control" }, "r",
        awesome.restart,
        {description = "reload awesome", group = "awesome"}
    ),
    awful.key(
        {modkey, "Shift"}, "q",
        awesome.quit,
        {description = "quit awesome", group = "awesome"}
    ),
    awful.key(
        {modkey}, "q",
        function() awful.spawn("suspend.sh") end,
        {description = "quit awesome", group = "awesome"}
    ),
    awful.key(
        {modkey}, "l",
        function() awful.tag.incmwfact( 0.05) end,
        {description = "increase master width factor", group = "layout"}
    ),
    awful.key(
        {modkey}, "h",
        function() awful.tag.incmwfact(-0.05)          end,
        {description = "decrease master width factor", group = "layout"}
    ),
    awful.key(
        {modkey, "Shift"}, "h",
        function() awful.tag.incnmaster(1, nil, true) end,
        {description = "increase the number of master clients", group = "layout"}
    ),
    awful.key(
        {modkey, "Shift"}, "l", 
        function() awful.tag.incnmaster(-1, nil, true) end,
        {description = "decrease the number of master clients", group = "layout"}
    ),
    awful.key(
        {modkey, "Control"}, "h",
        function() awful.tag.incncol( 1, nil, true) end,
        {description = "increase the number of columns", group = "layout"}
    ),
    awful.key(
        {modkey, "Control"}, "l",
        function() awful.tag.incncol(-1, nil, true) end,
        {description = "decrease the number of columns", group = "layout"}
    ),
    awful.key(
        {modkey}, "o",
        function() awful.layout.inc(1) end,
        {description = "select next", group = "layout"}
    ),
    awful.key(
        {modkey, "Shift"}, "o", function() awful.layout.inc(-1) end,
        {description = "select previous", group = "layout"}
    ),
    awful.key(
        {modkey, "Control"}, "n",
        function()
            local c = awful.client.restore()
            -- Focus restored client
            if c then
                c:emit_signal(
                    "request::activate", "key.unminimize", {raise = true}
                )
            end
        end,
        {description = "restore minimized", group = "client"}
    ),

    -- Prompt
    awful.key(
        {modkey}, "r",
        function()
            awful.screen.focused().prompt:run()
            awful.screen.focused().popup.visible = true
        end,
        {description = "run prompt", group = "launcher"}
    ),

    --[[
    awful.key(
        {modkey}, "x",
        function()
            awful.screen.focused().popup.visible = true
            awful.prompt.run {
                font = beautiful.font_prominent,
                prompt = "Run Lua code: ",
                textbox = awful.screen.focused().prompt.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir().."/history_eval",
                done_callback = function() 
                    awful.screen.focused().popup.visible = false
                end,
            }
        end,
        {description = "lua execute prompt", group = "awesome"}
    ),
    --]]
    -- Menubar
    --[[
    awful.key(
        {modkey}, "p",
        function() menubar.show() end,
        {description = "show the menubar", group = "launcher"}
    ),
    --]]
    awful.key(
        {modkey}, "space",
        mykeyboardlayout.next_layout,
        {description = "change language layout", group = "awesome"}
    ),
    awful.key(
        {}, "Pause",
        function() awful.spawn("mpc pause") end,
        {description = "pause music", group = "multimedia"}
    ),
    awful.key(
        {}, "XF86AudioLowerVolume",
        function() awful.spawn("amixer set Master 5%-") end,
        {description = "lower system volum", group = "multimedia"}
    ),
    awful.key(
        {}, "XF86AudioRaiseVolume",
        function() awful.spawn("amixer set Master 5%+") end,
        {description = "raise system volume", group = "multimedia"}
    ),
    awful.key(
        {}, "XF86AudioPlay",
        function() awful.spawn("mpc toggle") end,
        {description = "toggle music", group = "multimedia"}
    ),
    awful.key(
        {}, "XF86AudioNext",
        function() awful.spawn("mpc next") end,
        {description = "next song", group = "multimedia"}
    ),
    awful.key(
        {}, "XF86AudioPrev",
        function() awful.spawn("mpc prev") end,
        {description = "previous song", group = "multimedia"}
    )
)

clientkeys = gears.table.join(
    awful.key(
        {modkey}, "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}
    ),
    awful.key(
        {modkey, "Shift"}, "c",
        function(c) c:kill() end,
        {description = "close", group = "client"}
    ),
    awful.key(
        {modkey, "Shift"}, "f",
        awful.client.floating.toggle,
        {description = "toggle floating", group = "client"}
    ),
    awful.key(
        {modkey, "Control"}, "f",
        function(c) c.ontop = not c.ontop end,
        {description = "toggle floating", group = "client"}
    ),
    awful.key(
        {modkey, "Control"}, "Return",
        function(c) c:swap(awful.client.getmaster()) end,
        {description = "move to master", group = "client"}
    ),
    --[[
    awful.key(
        {modkey}, "o",
        function(c) c:move_to_screen() end,
        {description = "move to screen", group = "client"}
    ),
    awful.key(
        {modkey}, "t",
        function(c) c.ontop = not c.ontop end,
        {description = "toggle keep on top", group = "client"}
    ),
    --]]
    awful.key(
        {modkey}, "t",
        function(c) c.sticky = not c.sticky end,
        {description = "toggle sticky", group = "client"}
    ),
    awful.key(
        {modkey}, "n",
        function(c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end,
        {description = "minimize", group = "client"}
        ),
    awful.key(
        {modkey}, "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        {description = "(un)maximize", group = "client"}
    ),
    awful.key(
        {modkey, "Control"}, "m",
        function(c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end,
        {description = "(un)maximize vertically", group = "client"}
    ),
    awful.key(
        {modkey, "Shift"}, "m",
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end,
        {description = "(un)maximize horizontally", group = "client"}
    )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = gears.table.join(globalkeys,
        -- View tag only.
        awful.key(
            {modkey}, "#"..i+9,
            function()
                  local screen = awful.screen.focused()
                  local tag = screen.tags[i]
                  if tag then
                     tag:view_only()
                  end
            end,
            {description = "view tag #"..i, group = "tag"}
        ),
        -- Toggle tag display.
        awful.key(
            {modkey, "Control"}, "#"..i+9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            {description = "toggle tag #"..i, group = "tag"}
        ),
        -- Move client to tag.
        awful.key(
            {modkey, "Shift"}, "#"..i+9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
               end
            end,
            {description = "move focused client to tag #"..i, group = "tag"}
        ),
        -- Toggle tag on focused client.
        awful.key(
            {modkey, "Control", "Shift" }, "#"..i+9,
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            {description = "toggle focused client on tag #"..i, group = "tag"}
        )
    )
end

clientbuttons = gears.table.join(
    awful.button(
        {}, 1,
        function(c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
        end
    ),
    awful.button(
        {modkey}, 1,
        function(c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
            awful.mouse.client.move(c)
        end
    ),
    awful.button(
        {modkey}, 3,
        function(c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
            awful.mouse.client.resize(c)
        end
    )
)

-- Set keys
root.keys(globalkeys)

-- Rules ----------------------------------------------------------------------
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    {
        rule = {},
        properties = {
            border_width = beautiful.border_width,
            border_color = beautiful.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = clientkeys,
            buttons = clientbuttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen,
            -- screen = screen.primary, tag = screen.primary.tags[4]
        }
    },

    -- Floating clients.
    {
        rule_any = {
            instance = {"pinentry"},
            class = {"Tor Browser", "nemo"},
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {"Event Tester"},
            role = {
                "AlarmWindow",  -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = {floating = true}
    },

    -- Add titlebars to normal clients and dialogs
    --[[
    {
        rule_any = {type = {"normal", "dialog"}},
        properties = {titlebars_enabled = true}
    },
    --]]

    {
        rule = {class = "qutebrowser"},
        properties = {
            screen = screen.primary,
            tag = screen.primary.tags[1]
        },
    },
    {
        rule = {class = "thunderbird"},
        properties = {
            screen = screen.primary,
            tag = screen.primary.tags[2]
        },
    },
    {
        rule_any = {class = {"Anki", "calibre"}},
        properties = {screen = screen.primary, tag = screen.primary.tags[3]},
    },
    {
        rule = {class = "mpv"},
        properties = {screen = screen.primary, tag = screen.primary.tags[5]},
    },
    {
        rule = {name = "urxvt"},
        properties = {
            screen = screen.primary,
            tag = function()
                def = screen.primary.tags[6]
                local t = awful.screen.focused().selected_tag
                if not t then return def end
                if t.index >= 6 and t.index <= 9 then
                    return t
                end
                for i = 6, 9  do
                    t = screen.primary.tags[i]
                    if #t:clients() == 0 then
                        return t
                    end
                end
                return def
            end,
            switchtotag = true
        },
    },
    {
        rule_any = {
            name = {"popup_terminal"},
            class = {"Arandr", "Ristretto", "Nemo"}
        },
        properties = {
            floating = true,
            maximized = false,
            ontop = true,
            placement = awful.placement.centered,
        }
    },
    {
        rule_any = {
            name = {"popup_terminal"},
            class = {
                "Arandr",
                "Ristretto",
                "thunderbird",
                "qutebrowser",
                "firefox",
                "Thunar",
                "Anki",
                "calibre",
                "mpv",
            }
        },
        properties = {
            border_width = 0
        }
    },
}

-- Signals --------------------------------------------------------------------
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function(c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    if not awesome.startup then
        awful.client.setslave(c)
    end
    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = gears.table.join(
        awful.button(
            {}, 1,
            function()
                c:emit_signal("request::activate", "titlebar", {raise = true})
                awful.mouse.client.move(c)
            end
        ),
        awful.button(
            {}, 3,
            function()
                c:emit_signal("request::activate", "titlebar", {raise = true})
                awful.mouse.client.resize(c)
            end
        )
    )

    awful.titlebar(c):setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal(
    "mouse::enter",
    function(c)
        c:emit_signal("request::activate", "mouse_enter", {raise = false})
    end
)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- Notifications --------------------------------------------------------------
naughty.config.spacing = 20
naughty.config.defaults.border_width = 0
naughty.config.defaults.margin = 10
naughty.config.defaults.width = 300
naughty.config.defaults.timeout = 60
naughty.config.defaults.offset = 100
naughty.config.presets.critical.bg = beautiful.fg_urgent
naughty.config.padding = 30

