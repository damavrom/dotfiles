if &compatible
  set nocompatible
endif

set backspace=indent,eol,start
set ruler
set suffixes+=.aux,.bbl,.blg,.brf,.cb,.dvi,.idx,.ilg,.ind,.inx,.jpg,.log,.out,.png,.toc
set suffixes-=.h
set suffixes-=.obj

" Move temporary files to a secure location to protect against CVE-2017-1000382
if exists('$XDG_CACHE_HOME')
  let &g:directory=$XDG_CACHE_HOME
else
  let &g:directory=$HOME . '/.cache'
endif
let &g:undodir=&g:directory . '/vim/undo//'
let &g:backupdir=&g:directory . '/vim/backup//'
let &g:directory.='/vim/swap//'
" Create directories if they doesn't exist
if ! isdirectory(expand(&g:directory))
  silent! call mkdir(expand(&g:directory), 'p', 0700)
endif
if ! isdirectory(expand(&g:backupdir))
  silent! call mkdir(expand(&g:backupdir), 'p', 0700)
endif
if ! isdirectory(expand(&g:undodir))
  silent! call mkdir(expand(&g:undodir), 'p', 0700)
endif

" Make shift-insert work like in Xterm
if has('gui_running')
  map <S-Insert> <MiddleMouse>
  map! <S-Insert> <MiddleMouse>
endif

syntax on   "highlight code syntax
set relativenumber  "show how far each line is from cursor
set number  "number current line
set tabstop=4   "size of tab
set shiftwidth=4    "size of `>>`
set smartindent "auto add spaces
"set nowrap "don't wrap lines
set expandtab   "spaces for tab
"set hlsearch   "highlight search matches
"set ignorecase "ignore case in search
set visualbell  "flash instead of beeping
set scrolloff=10    "keep cursor 10 lines away from beginning of line
set incsearch   "move cursor to ocurance while still typing the search 
                "term
set wildmenu    "show suggestions in commands with tab pressing
"set notitle    "don't display file info in window title
set confirm "ask for confirmation to close an unsaved file
set shell=/bin/bash "set shell for commands
set titlestring=%f\ -\ VIM "set window title
set backupcopy=yes "this makes parcel-bundler rebuild when code is changed
set background=dark "for dark terminals
set colorcolumn=73,80 "maximum suggested line width
set nowrapscan "don't circle search
set hlsearch

inoremap { {}<Esc>ha
inoremap ( ()<Esc>ha
inoremap [ []<Esc>ha
inoremap " ""<Esc>ha
inoremap ' ''<Esc>ha

colorscheme desert
"highlight the line with the cursor
set cursorline
hi Normal ctermbg=None
hi NonText ctermbg=NONE
hi LineNR ctermfg=242
hi StatusLine ctermfg=245 ctermbg=none
hi CursorLine cterm=NONE ctermbg=237
hi CursorLineNr ctermfg=245
hi ColorColumn ctermbg=131 ctermfg=none

""" Filetype specific settings """
autocmd FileType html setlocal shiftwidth=2 softtabstop=2
autocmd FileType css setlocal shiftwidth=2 softtabstop=2
autocmd FileType make setlocal noexpandtab


""" Greek Support """
set keymap=greek "greek keyboard for insert mode
set iminsert=0 "make greek not default for insert
set imsearch=0 "make greek not default for searching
set spelllang=en,el "add greek to spell utils

""" Status Bar """
set laststatus=2    "display status bar always
set statusline=
"set statusline+=%7*\[%n]                                  "buffernr
set statusline+=\ %<%f                                   "File+path
set statusline+=%m%r%w\                      "Modified? Readonly? Top/bot.
set statusline+=\ %y\                                  "FileType
set statusline+=\ %{''.(&fenc!=''?&fenc:&enc).''}      "Encoding
set statusline+=\ %{(&bomb?\",BOM\":\"\")}\            "Encoding2
set statusline+=\ %{&ff}\                              "FileFormat (dos/unix..) 
set statusline+=\ %{&spelllang}\                       "Spellanguage & Highlight on?
"set statusline+=%8*\ %=\ row:%l/%L\ (%p%%)\             "Rownumber/total (%)
"set statusline+=%8*\ column:%v\              "Rownumber/total (%)
"set statusline+=%8*\ %=\ %l:%v\              "Rownumber/total (%)
set statusline+=%=
set statusline+=\ w:%{wordcount().words}
set statusline+=\ %P\                       "Modified? Readonly? Top/bot.

call vundle#begin()
Plugin 'junegunn/goyo.vim'
Plugin 'preservim/vim-pencil'
call vundle#end()
