#!/bin/bash

# NAME
#   sync.sh - synchronisation utility script
#
# DESCRIPTION
#   Show the unsynchronised dotfiles and perform synchronisation
#   operations.
#
# SYNOPSIS
#   sync.sh [interactive]
#
# OPTIONS
#   interactive
#       Have an interactive session and be asked what to do with each
#       dotifile.


[ "$1" == interactive ] && interactive=true || interactive=false

excludefile=~/.local/share/exclude_dotfiles 

exclude=
[ -f $excludefile ] && \
    exclude=$(sed 's/#.*$//' $excludefile | tr -d '[ \t]')

files=$(git ls-files home | sed 's/^home\///')
for file in $files; do
    [ -n "$(echo "$exclude" | grep "^$file$")" ] && continue
    if [ ! -f ~/$file ]; then
        if $interactive; then
            echo
            echo "------------------------------------"
            echo "absent:    ~/$file"
            echo action?
            echo [c] copy to computer
            echo [i] add to ignore list
            echo "[N] don't take action"
            read -n 1 -r
            echo
            if [[ $REPLY =~ ^[Cc]$ ]]; then
                dir=~/$file
                dir=${dir%/*}
                [ ! -d $dir ] && mkdir -p $dir
                cp -v home/$file ~/$file
            fi
            [[ $REPLY =~ ^[Ii]$ ]] && echo $file >> $excludefile
        else
            echo "absent:    ~/$file"
        fi
    elif [ -n "$(diff -q ~/$file home/$file)" ]; then 
        if $interactive; then
            echo
            echo "------------------------------------"
            echo "changed:   ~/$file"
            flag=true
            while $flag; do
                flag=false
                echo action?
                echo [c] copy to computer
                echo [r] copy to repository
                echo [d] show diff
                echo [i] add to ignore list
                echo "[N] don't take action"
                read -n 1 -r
                echo
                [[ $REPLY =~ ^[Cc]$ ]] && cp -v home/$file ~/$file
                [[ $REPLY =~ ^[Rr]$ ]] && cp -v ~/$file home/$file
                if [[ $REPLY =~ ^[Dd]$ ]]; then
                    echo
                    echo "------------------------------------"
                    diff -u ~/$file home/$file
                    echo "------------------------------------"
                    flag=true
                fi
                [[ $REPLY =~ ^[Ii]$ ]] && echo $file >> $excludefile
            done
        else
            echo "changed:   ~/$file"
        fi
    fi
done
