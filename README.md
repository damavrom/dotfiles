# Dotfiles
This repo serves for simplifying inter-machine configuration and for
making all of my configuration files available to the public. All of the
files are placed in the [home] directory and their names and paths are
as if they were in my `$HOME` directory.

#### Contents
* [Visual Preview]
* [Setup Summary]
* [Productivity Profile]
  * [Software]
  * [Hardware]
* [See Also]

#### Installation
There is a script to list all the absent or modified files there are in
the system. This is the `sync.sh` script. I run this to get a full
list of all the files that are in the repo but not on my system or the
files that have been changed either on my system or on the repo, then I
decide for each file if I want to overwrite it on my system or on the repo.

## Visual Preview
> **NOTE**: These screenshots are outdated.
#### Left desktop monitor
![left desktop monitor]

#### Right desktop monitor
![right desktop monitor]

#### Laptop monitor
![laptop monitor]

## Setup Summary

| Software Type       | Desktop                  | Laptop                |
| :------------------ | :----------------------- | :-------------------- |
| Linux Distribution  | [Arch Linux]             | [Arch Linux]          |
| Window Manager      | [awesome]                | [awesome]             |
| Web Browser         | [qutebrowser]            | [qutebrowser]         |
| Music Player Daemon | [MPD]                    | -                     |
| Music Player Client | [ncmpc]                  | [mpv]                 |
| Video Player        | [mpv]                    | [mpv]                 |
| File Manager        | [GNOME Files]            | -                     |
| Image Viewer        | [sxiv]                   | [sxiv]                |
| PDF Viewer          | [MuPDF]                  | [MuPDF]               |
| Document Editor     | [LibreOffice]/[TeX Live] | [TeX Live]            |
| Screen Shooter      | [scrot]                  | [scrot]               |
| Terminal Emulator   | [rxvt-unicode]           | [rxvt-unicode]        |
| Shell               | [Bash]                   | [Bash]                |
| Text Editor         | [Vim]                    | [Vim]                 |
| USB Manager         | [udiskie]                | -                     |
| e-mail Client       | [Thunderbird]            | -                     |
| AUR Manager         | [Auracle]                | [Auracle]             |

#### Honorable Mentions
* [Anki]
* [Git]
* [Redshift]
* [youtube-dl]
* [ImageMagick]
* [GIMP]
* [p7zip]
* the rest of [GNU] packages

## Productivity Profile
After a long time of exposure to the open source ecosystem and community
and pushed by the need for improved productivity I have come to use
specific pieces of software. The basic notions behind the software I use
are **philosophy** and **productivity**. Although after being eye candy
hobbyist I consider appearance sometimes also important.

Productivity wise, I choose to use tools because of their efficiency and
not their ease of use. First, I want every program I use to be launched
and respond instantly to anything I do. All of the applications I use
comply to this and it frustrate me a lot if I have to use any software
that is inexplicably slow. Second, I want the program to allow me be the
most efficient I can, I don't want programs to limit my potential. I
don't mind the learning curve as long as it is rewarding.

Philosophy wise, I choose to use free and open source programs. I want
the programs I use to be mine and serve me. For more information about
this subject you can refer to [wikipedia].

### Software
#### Operating System
The operating system I use for the last few years is **Arch Linux**.
This is a Linux distribution focused on simplicity. Like many Linux
enthusiasts I have experimented with many distributions before this one.
I eventually started using it exclusively because of various reasons.
It's a rolling release distribution, meaning I don't have to upgrade it
every year. The documentation never fails to educate me and give me
insights on troubleshooting. The package manager is very easy to learn
and very consistent with the installed packages. The available packages
are vast and always up to date. I have never in my years of use
experienced issues regarding the distribution itself.

In my opinion, Arch Linux is certainly a quality desktop operating
system.

#### Window Manager
> 2021 update: At this point I have concluded that no existing WM fits
exactly my needs. Hence I keep using what I use without investing much
time on reconfiguring. This is the reason the WM dotfiles may look
messy. I have conluded that the WM I would want to use uses a widespred
configuration namespace and not some lousy and chaotic namespace in
attemt of building it from the ground up. I also want it to not be
dependent on severily outdated technologies like _Xorg_. I have some
more thoughts on this and I even have the ambition of starting to
develop one as a hobby. If you want to boost the development or provide
any kind of help you can contact me.

On both my desktop and laptop I run **awesome**.

_awesome_ is a very fast, light, tiling window managers that use _Vim_
keybindings. I have been using _i3_ and _BSPWM_ for a long time and I
decided to try something new, so this is why I installed and attempted
to configure _awesomewm_ just to see how it can effect my productivity
flow. My configuration is _still_ in a pilot stage.

#### Shell
**Bash** is my shell of choice because I know it better than the other
scripting languages.

I could have been using _zsh_ instead but the fact that bash is default
in most distributions and more widespread than zsh overweights the few
benefits of learning it and using it.

Bash is also a member of the _GNU_ family and this makes me a little bit
more drawn to it.

#### Text Editor
For the time being, I use **Vim** as my text editor, config editor, and
IDE. I believe I will be using for long time to come because of how it
unleashes my potential of programming efficiently. After years of using
it, I constantly learn new tricks and ways to hack my way to better text
editing. Also being command line program makes it faster, lighter and
more portable.

I use Vim vanilla without any plugins on, apart from language
dictionaries. I use it so because it makes me learn and use the build in
commands to their full potential.


#### Terminal Emulator
I use **Urxvt** as my terminal. I don't have a particular reason. Until
now I haven't had any trouble configuring it the way I want so I haven't
made a change to another terminal, but I am willing to discover new
terminals.

#### Web Browser
My browser is **Qutebrowser**. I have been using a browser almost as
long as I have been using computers and I have tried numerous. I came to
choose Qutebrowser because it complies with the philosophy of the rest
programs I use. It uses vim keybindings natively and it is actually
developed around offering productivity. There are other browsers that
enable vim keybinding with plug-ins but these browsers are in the core
developed to offer ease of use to the newcomer user.

Qutebrowser is not the fastest browser there is, other browsers load
pages slightly faster, but I am the fastest I can be with it, so in its
case I trade the efficiency of the program with how efficient I am.

#### System Monitoring
**Polybar** is the piece of software that I look at to know various
information. It is fast, light, easily configurable and portable. It
runs on both the window managers I am currently using so it spares me
from the trouble of tweaking with a new one.

### Hardware
Right now I own two computers, a desktop and a laptop.

My **desktop** is a computer I put together myself in 2016. Its
processor is an Intel Core i5-6600, it has 4 cores that run up to the
speed of 3.9 GHz. The memory is 8Gb total at the speed of 2.1GHz. My
disks are an SSD and an HDD. Currently I use a dual monitor as it is
shown by the screenshots. For graphics I use the integrated graphics of
the CPU.

My **laptop** is a ThinkPad Edge 11". Even though it was intorduced to
the market in 2010 it is still a machine that fits my needs very well
and as long it functions normally I don't have any plans on replacing
it.

## See Also
* [shell scripts], a collection of scripts I wrote.

[Visual Preview]: #visual-preview
[Setup Summary]: #setup-summary
[Productivity Profile]: #productivity-profile
[Software]: #software
[Hardware]: #hardware
[See Also]: #see-also

[home]: home
[left desktop monitor]: desktop-left.png
[right desktop monitor]: desktop-right.png
[laptop monitor]: laptop.png

[Arch Linux]: https://www.archlinux.org/
[bspwm]: https://github.com/baskerville/bspwm
[i3]: https://i3wm.org/
[awesome]: https://awesomewm.org/
[qutebrowser]: https://qutebrowser.org/
[Rofi]: https://github.com/DaveDavenport/rofi
[Polybar]: https://github.com/polybar/polybar
[MPD]: https://www.musicpd.org/
[ncmpc]: https://www.musicpd.org/clients/ncmpc/
[mpv]: https://mpv.io/
[GNOME Files]: https://wiki.gnome.org/Apps/Files
[sxiv]: https://github.com/muennich/sxiv
[MuPDF]: https://mupdf.com/
[TeX Live]: http://tug.org/texlive/
[scrot]: https://github.com/resurrecting-open-source-projects/scrot
[rxvt-unicode]: http://software.schmorp.de/pkg/rxvt-unicode.html
[Bash]: https://www.gnu.org/software/bash/bash.html
[Vim]: https://www.vim.org
[i3lock]: https://i3wm.org/i3lock/
[udiskie]: https://pypi.python.org/pypi/udiskie
[Dunst]: https://dunst-project.org/
[Thunderbird]: https://www.mozilla.org/thunderbird/
[Mutt]: http://www.mutt.org/
[Auracle]: https://github.com/falconindy/auracle
[Anki]: https://ankisrs.net/
[Git]: https://git-scm.com/
[Redshift]: http://jonls.dk/redshift/
[youtube-dl]: https://rg3.github.io/youtube-dl/
[ImageMagick]: https://www.imagemagick.org/
[GIMP]: https://www.gimp.org/
[LibreOffice]: https://www.libreoffice.org/
[Firefox]: https://www.mozilla.org/firefox/
[p7zip]: http://p7zip.sourceforge.net/
[GNU]: https://www.gnu.org/
[wikipedia]: https://en.wikipedia.org/wiki/Free_software
[shell scripts]: https://gitlab.com/damavrom/shell-scripts
